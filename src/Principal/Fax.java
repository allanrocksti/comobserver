package Principal;

import java.util.Observable;
import java.util.Observer;

public class Fax implements Observer{
	
	private String fabricante;
	private int codigo;
	
	public Fax(String fabricante, int codigo) {
		super();
		this.fabricante = fabricante;
		this.codigo = codigo;
	}
	
	public void telefoneAtendido(Telefone telefone) {
		// Comportamento espec�fico para um fax!
		System.out.println("Fax [" + this.getCodigo() + "]: Fiquei sabendo que o telefone "
				+ telefone.getNumero() + " foi atendido!");
	}

	public void telefoneDesligado(Telefone telefone) {
		// Comportamento espec�fico para um fax!
		System.out.println("Fax [" + this.getCodigo() + "]: Fiquei sabendo que o telefone "
				+ telefone.getNumero() + " foi desligado!");
	}

	public void telefoneTocou(Telefone telefone) {
		// Comportamento espec�fico para um fax!
		System.out.println("Fax [" + this.getCodigo() + "]: Fiquei sabendo que o telefone "
				+ telefone.getNumero() + " tocou!");
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		Telefone t = (Telefone) arg0;
		String acao = String.valueOf(arg1);
		
		if (acao.equals("tocar")) {
			telefoneTocou(t);
		} else if(acao.equals("atender")) {
			telefoneAtendido(t);
		} else if(acao.equals("desligar")) {
			telefoneDesligado(t);
		}
	
	}
	
	
	// getters e setters
	
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
}