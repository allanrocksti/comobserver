package Principal;
// import java.util.HashSet;
import java.util.Observable;
// import java.util.Set;


public class Telefone extends Observable implements Mensagens{

	private String numero;
	private String acao;
	
	// ...
	
	//private Set<Pessoa> pessoas;
	//private Set<SecretariaEletronica> secretarias;
	//private Set<Fax> faxes;

	public Telefone() {
		super();
		//this.pessoas = new HashSet<Pessoa>();
		//this.secretarias = new HashSet<SecretariaEletronica>();
	   //this.faxes = new HashSet<Fax>();
	}
	
	public Telefone(String numero) {
		this();
		this.numero = numero;
	}
	
	public void mudarEstado() {
		setChanged();
		notifyObservers(acao);
	}

	@Override
	public void tocar() {
		this.acao = "tocar";
		mudarEstado();
		
	}

	@Override
	public void atender() {
		this.acao = "atender";
		mudarEstado();
	}

	@Override
	public void desligar() {
		// TODO Auto-generated method stub
		this.acao = "desligar";
		mudarEstado();
	}

	/*public void tocar() {
		// Coisas para o telefone tocar!!!
		
		System.out.println("Telefone " + this.getNumero() 
				+ " est� tocando!");		
		for (Pessoa pessoa : pessoas) {
			pessoa.telefoneTocou(this);
		}
		for (SecretariaEletronica secretaria : secretarias) {
			secretaria.telefoneTocou(this);
		}
		for (Fax fax : faxes) {
			fax.telefoneTocou(this);
		}
	}
	
	public void atender() {
		// Coisas para atender o telefone!!!
		
		for (Pessoa pessoa : pessoas) {
			pessoa.telefoneAtendido(this);
		}
		for (SecretariaEletronica secretaria : secretarias) {
			secretaria.telefoneAtendido(this);
		}
		for (Fax fax : faxes) {
			fax.telefoneAtendido(this);
		}
	}
	
	public void desligar() {
		// Coisas para desligar o telefone!!!
		
		for (Pessoa pessoa : pessoas) {
			pessoa.telefoneDesligado(this);
		}
		for (SecretariaEletronica secretaria : secretarias) {
			secretaria.telefoneDesligado(this);
		}
		for (Fax fax : faxes) {
			fax.telefoneDesligado(this);
		}
	}
	
	
	
	public void addPessoa(Pessoa pessoa) {
		this.pessoas.add(pessoa);
	}
	
	public void removePessoa(Pessoa pessoa) {
		this.pessoas.remove(pessoa);
	}
	
	public void addSecretaria(SecretariaEletronica pessoa) {
		this.secretarias.add(pessoa);
	}
	
	public void removeSecretaria(SecretariaEletronica pessoa) {
		this.secretarias.remove(pessoa);
	}
	
	public void addFax(Fax fax) {
		this.faxes.add(fax);
	}
	
	public void removeFax(Fax fax) {
		this.faxes.remove(fax);
	}*/
	
	// Getters e setters
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	
}
