package Principal;

import java.util.Observable;
import java.util.Observer;

public class Pessoa implements Observer{

	private String nome;
	private String cpf;

	public Pessoa(String nome, String cpf) {
		super();
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public void telefoneAtendido(Telefone telefone) {
		// Comportamento espec�fico para uma pessoa!
		System.out.println("Pessoa [" + this.getNome() + "]: Fiquei sabendo que o telefone "
				+ telefone.getNumero() + " foi atendido!");
	}

	public void telefoneDesligado(Telefone telefone) {
		// Comportamento espec�fico para uma pessoa!
		System.out.println("Pessoa [" + this.getNome() + "]: Fiquei sabendo que o telefone "
				+ telefone.getNumero() + " foi desligado!");
	}

	public void telefoneTocou(Telefone telefone) {
		// Comportamento espec�fico para uma pessoa!
		System.out.println("Pessoa [" + this.getNome() + "]: Fiquei sabendo que o telefone "
				+ telefone.getNumero() + " tocou!");	
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		Telefone t = (Telefone) arg0;
		String acao = String.valueOf(arg1);
		
		if(acao.equals("tocar")) {
			telefoneTocou(t);
		} else if(acao.equals("atender")) {
			telefoneAtendido(t);
		} else if(acao.equals("desligar")) {
			telefoneDesligado(t);
		}
		
	}
	
	
	// Getters e setters
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
