package Principal;

import java.util.Observable;
import java.util.Observer;

public class SecretariaEletronica implements Observer{

	private String numeroSerie;
	private String modelo;

	public SecretariaEletronica(String numeroSerie, String modelo) {
		super();
		this.numeroSerie = numeroSerie;
		this.modelo = modelo;
	}

	public void telefoneAtendido(Telefone telefone) {
		// Comportamento espec�fico para uma secret�ria eletr�nica!
		System.out.println("Secretaria [" + this.getNumeroSerie() + "]: Fiquei sabendo que o telefone "
				+ telefone.getNumero() + " foi atendido!");	
	}
	
	public void telefoneDesligado(Telefone telefone) {
		// Comportamento espec�fico para uma secret�ria eletr�nica!
		System.out.println("Secretaria [" + this.getNumeroSerie() + "]: Fiquei sabendo que o telefone "
				+ telefone.getNumero() + " foi desligado!");		
	}

	public void telefoneTocou(Telefone telefone) {
		// Comportamento espec�fico para uma secret�ria eletr�nica!
		System.out.println("Secretaria [" + this.getNumeroSerie() + "]: Fiquei sabendo que o telefone "
				+ telefone.getNumero() + " tocou!");		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		Telefone t = (Telefone) arg0;
		String acao = String.valueOf(arg1);
		
		if(acao.equals("tocar")) {
			telefoneTocou(t);
		} else if(acao.equals("atender")) {
			telefoneAtendido(t);
		} else if(acao.equals("desligar")) {
			telefoneDesligado(t);
		}
	
	}
	
	
	// Getters e setters
	
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
}
