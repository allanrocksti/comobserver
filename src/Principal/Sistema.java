package Principal;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;


public class Sistema {

	private static Scanner scanner;

	public static void main(String[] args) {
		System.out.println("Versão COM Observer. \n");

		Observer pJoao = new Pessoa("Jo�o", "12345678");
		Observer pMaria = new Pessoa("Maria", "98765432");
		
		Observer sABC = new SecretariaEletronica("ABC", "X25");
		
		Observer fax123 = new Fax("Motorola", 123456);
		
		Observable telefone1 = new Telefone("9999-9999");
		Observable telefone2 = new Telefone("8888-8888");
		
		telefone1.addObserver(pJoao);
		telefone1.addObserver(pMaria);
		telefone1.addObserver(sABC);
		//telefone1.addPessoa(pJoao);
		//telefone1.addPessoa(pMaria);
		//telefone1.addSecretaria(sABC);
		
		telefone2.addObserver(pJoao);
		telefone2.addObserver(fax123);
		//telefone2.addPessoa(pJoao);
		//telefone2.addFax(fax123);
		
		scanner = new Scanner(System.in);
		
		String cmd = "";
		
		while (!cmd.equalsIgnoreCase("exit")) {
			System.out.print("Digite o comando: ");
			cmd = scanner.nextLine();
			
			String[] cmdSplited = cmd.split(" ");
			
			if (cmdSplited.length == 2) {				
				Telefone t = (Telefone) telefone1;
				if (cmdSplited[1].equals("2"))
					t = (Telefone) telefone2;
				
				if (cmdSplited[0].equalsIgnoreCase("t")) {
					t.tocar();
				} else if (cmdSplited[0].equalsIgnoreCase("a")) {
					t.atender();
				} else if (cmdSplited[0].equalsIgnoreCase("d")) {
					t.desligar();
				}
			}
				
		}
		
		System.out.println("Good by!");
		
	}

}
